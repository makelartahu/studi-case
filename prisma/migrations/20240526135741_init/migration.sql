-- CreateTable
CREATE TABLE `Role` (
    `id_role` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL,

    PRIMARY KEY (`id_role`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `User` (
    `id_user` INTEGER NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(45) NULL,
    `password` VARCHAR(100) NULL,
    `nama` VARCHAR(45) NULL,
    `token` VARCHAR(45) NULL,
    `roleId` INTEGER NOT NULL,

    UNIQUE INDEX `User_token_key`(`token`),
    PRIMARY KEY (`id_user`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Order` (
    `id_order` INTEGER NOT NULL AUTO_INCREMENT,
    `nama` VARCHAR(45) NULL,
    `email` VARCHAR(45) NULL,
    `upload_identitas` MEDIUMTEXT NULL,
    `kota` VARCHAR(45) NULL,
    `kecamatan` VARCHAR(45) NULL,
    `jalan` VARCHAR(45) NULL,
    `reject_reason` VARCHAR(45) NULL,
    `paket_id` INTEGER NULL,
    `status_id` INTEGER NULL,
    `userId_user` INTEGER NULL,
    `teknisi_id` INTEGER NULL,

    INDEX `paket_id_idx`(`paket_id`),
    INDEX `userId_user_idx`(`userId_user`),
    PRIMARY KEY (`id_order`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Paket` (
    `id_paket` INTEGER NOT NULL AUTO_INCREMENT,
    `nama_paket` VARCHAR(45) NULL,
    `harga` INTEGER NULL,
    `deskripsi` VARCHAR(100) NULL,
    `jumlah_penjualan` INTEGER NULL,

    PRIMARY KEY (`id_paket`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Teknisi` (
    `id_teknisi` INTEGER NOT NULL AUTO_INCREMENT,
    `nama_teknisi` VARCHAR(45) NULL,
    `nomor_telepon` VARCHAR(45) NULL,
    `nip` VARCHAR(45) NULL,
    `total_handling` VARCHAR(45) NULL,

    UNIQUE INDEX `Teknisi_nip_key`(`nip`),
    PRIMARY KEY (`id_teknisi`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `User` ADD CONSTRAINT `User_roleId_fkey` FOREIGN KEY (`roleId`) REFERENCES `Role`(`id_role`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Order` ADD CONSTRAINT `Order_paket_id_fkey` FOREIGN KEY (`paket_id`) REFERENCES `Paket`(`id_paket`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Order` ADD CONSTRAINT `Order_userId_user_fkey` FOREIGN KEY (`userId_user`) REFERENCES `User`(`id_user`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Order` ADD CONSTRAINT `Order_teknisi_id_fkey` FOREIGN KEY (`teknisi_id`) REFERENCES `Teknisi`(`id_teknisi`) ON DELETE SET NULL ON UPDATE CASCADE;
