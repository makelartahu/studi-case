import { ResponseHandler} from "../response/responseHandler.js";

export const errorMiddleware = (error, req, res, next) => {
  if (!error) {
    next();
    return;
  }

  const status = error.status || 500;
  const message = error instanceof Error ? error.message : error;

  ResponseHandler.error(res, message, status);
}