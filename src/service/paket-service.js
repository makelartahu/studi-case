import { prismaClient } from "../application/database.js";

export const getPaketList = async () => {
	try {
		return await prismaClient.paket.findMany();
	} catch (error) {
		throw new Error(
			`Error occurred while fetching paket list: ${error.message}`,
		);
	}
};
export const getPaketListDesc = async () => {
	try {
		return await prismaClient.paket.findMany({
			orderBy: {
				jumlah_penjualan: "desc",
			},
		});
	} catch (error) {
		throw new Error(
			`Error occurred while fetching paket list: ${error.message}`,
		);
	}
};
