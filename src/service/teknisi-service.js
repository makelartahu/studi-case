import { prismaClient } from "../application/database.js";
import { validate } from "../validation/validation.js";
import { teknisiValidation } from "../validation/teknisi-validation.js";

export const getTeknisi = async (request) => {
	const teknisi = validate(teknisiValidation, request);

	const result = await prismaClient.teknisi.findUnique({
		where: {
			id_teknisi: teknisi.id,
		},
	});

	return {
		data: {
			id: result.id_teknisi,
			nama: result.nama_teknisi,
			nomor_telepon: result.nomor_telepon,
			nip: result.nip,
		},
	};
};

export const getTeknisiList = async () => {
	try {
		return await prismaClient.teknisi.findMany({
			orderBy: {
				total_handling: "desc",
			},
		});
	} catch (error) {
		throw new Error(
			`Error occurred while fetching paket list: ${error.message}`,
		);
	}
};
