import { prismaClient } from "../application/database.js";
import { validate } from "../validation/validation.js";
import { loginValidation, userValidation } from "../validation/user-validation.js";
import {
	hashPassword,
	searchUserByUsername,
	countUserByUser,
	comparePassword,
	generatedToken,
	updateUserToken,
	searchUserByToken,
} from "./utility-service.js";
import jwt from "jsonwebtoken";

export const createUser = async (request) => {
	const user = validate(userValidation, request);

	const userExists = await countUserByUser(user.username);

	if (userExists > 0) {
		throw new Error("User already exists");
	}

	const hashedPassword = await hashPassword(user.password);

	await prismaClient.user.create({
		data: {
			username: user.username,
			password: hashedPassword,
			nama: user.nama,
			roleId: user.roleId,
		},
	});

	return {
		username: user.username,
		nama: user.nama,
		roleId: user.roleId,
	};
};

export const loginUser = async (request) => {
	const user = await validate(loginValidation, request);

	const userExists = await searchUserByUsername(user.username);

	if (!userExists) {
		throw new Error("User not found");
	}

	const passwordMatch = await comparePassword(user.password, userExists.password);

	if (!passwordMatch) {
		throw new Error("Password does not match");
	}

	const payload = {
		id: userExists.id_user,
		email: userExists.email,
		nama: userExists.nama,
		password: userExists.password,
	};

	const secret = process.env.JWT_SECRET;
	const token = jwt.sign(payload, secret, { expiresIn: "1h" });

	return {
		id: userExists.id_user,
		nama: userExists.nama,
		token: token,
	};
};

export const getUserByToken = async (token) => {
	const user = await searchUserByToken(token);
	if (!user) {
		throw new Error("Invalid token");
	}

	return {
		id: user.id_user,
		nama: user.nama,
		email: user.email,
		kota: user.kota,
		kecamatan: user.kecamatan,
		jalan: user.jalan,
		roleId: user.roleId,
	};
};

export const logoutUser = async (request) => {
	const user = await searchUserByUsername(request.username);
	if (!user) {
		throw new Error("Invalid username");
	}

	const insertToken = await updateUserToken(user.id_user, null);

	if (!insertToken) {
		throw new Error("Failed to insert token");
	}

	return {
		id: user.id_user,
		nama: user.nama,
	};
};
