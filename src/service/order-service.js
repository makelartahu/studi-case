import {
	orderSearchValidation,
	orderUpdateValidation,
	orderValidation,
} from "../validation/order-validation.js";
import { validate } from "../validation/validation.js";
import { prismaClient } from "../application/database.js";
import {
	searchOrderIdByEmail,
	searchPaketById,
	searchTeknisiByNama,
	searchUserByToken,
} from "./utility-service.js";
import e from "express";

export const createOrder = async (request, token) => {
	const order = validate(orderValidation, request);
	const user = await searchUserByToken(token);
	const paket = await searchPaketById(order.paket_id);

	if (!user) {
		throw new Error("User not found");
	}

	const orderExist = await prismaClient.order.count({
		where: {
			email: order.email,
		},
	});

	if (orderExist > 0) {
		throw new Error("Order already exists");
	}

	const newOrder = await prismaClient.order.create({
		data: {
			nama: user.nama,
			email: order.email,
			upload_identitas: order.upload_identitas,
			kota: order.kota,
			kecamatan: order.kecamatan,
			jalan: order.jalan,
			paket_id: order.paket_id,
			status_id: 0,
			userId_user: user.id_user,
		},
	});

	return {
		data: {
			order_id: newOrder.id_order,
			nama_paket: paket.nama_paket,
		},
	};
};

export const updateOrder = async (request) => {
	const order = validate(orderUpdateValidation, request);
	const paket = await searchPaketById(order.paket_id);
	const teknisi = await searchTeknisiByNama(order.nama_teknisi);
	const orderId = await searchOrderIdByEmail(order.email);

	if (!paket) {
		paket = orderId.paket_id;
	}
	if (!teknisi) {
		teknisi = null;
	}
	await prismaClient.order.updateMany({
		where: {
			email: order.email,
		},
		data: {
			nama: order.nama,
			email: order.email,
			upload_identitas: order.upload_identitas,
			kota: order.kota,
			kecamatan: order.kecamatan,
			jalan: order.jalan,
			paket_id: order.paket_id,
			status_id: order.status_id,
			teknisi_id: teknisi.id_teknisi,
			reject_reason: order.reject_reason,
		},
	});

	return {
		data: {
			id: orderId.id_order,
			nama_paket: paket.nama_paket,
		},
	};
};

export const getOrder = async (request) => {
	const order = validate(orderSearchValidation, request);

	const result = await prismaClient.order.findUnique({
		where: {
			id_order: order.id,
		},
	});

	return {
		data: {
			id: result.id_order,
			nama: result.nama,
			email: result.email,
			upload_identitas: result.upload_identitas,
			kota: result.kota,
			kecamatan: result.kecamatan,
			jalan: result.jalan,
			user_id: result.userId_user,
			paket_id: result.paket_id,
			status_id: result.status_id,
			teknisi_id: result.teknisi_id,
		},
	};
};
export const getOrderList = async () => {
	const result = await prismaClient.order.findMany({
		orderBy: {
			status_id: "desc",
		},
	});

	return {
		data: result.map((order) => ({
			id: order.id_order,
			nama: order.nama,
			email: order.email,
			upload_identitas: order.upload_identitas,
			kota: order.kota,
			kecamatan: order.kecamatan,
			jalan: order.jalan,
			user_id: order.userId_user,
			paket_id: order.paket_id,
			status_id: order.status_id,
			teknisi_id: order.teknisi_id,
		})),
	};
};
