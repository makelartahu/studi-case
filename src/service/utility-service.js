import { prismaClient } from "../application/database.js";
import bcrypt from "bcrypt";
import { v4 as uuid } from "uuid";
import jwt from "jsonwebtoken";

export const hashPassword = async (password) => {
	try {
		return await bcrypt.hash(password, 10);
	} catch (error) {
		throw new Error(`Error occurred while hashing password: ${error.message}`);
	}
};

export const comparePassword = async (password, hash) => {
	try {
		return await bcrypt.compare(password, hash);
	} catch (error) {
		throw new Error(`Error occurred while comparing password: ${error.message}`);
	}
};

export const searchUserByUsername = async (username) => {
	try {
		return await prismaClient.user.findFirst({
			where: {
				username: username,
			},
		});
	} catch (error) {
		throw new Error(`Error occurred while searching user by username: ${error.message}`);
	}
};

export const searchUserByToken = async (token) => {
	try {
		const decoded = jwt.verify(token, process.env.JWT_SECRET);
		return await prismaClient.user.findFirst({
			where: {
				id_user: decoded.id,
			},
		});
	} catch (error) {
		throw new Error(`Error occurred while searching user by token: ${error.message}`);
	}
};

export const countUserByUser = async (username) => {
	try {
		return await prismaClient.user.count({
			where: {
				username: username,
			},
		});
	} catch (error) {
		throw new Error(`Error occurred while counting user by username: ${error.message}`);
	}
};

export const generatedToken = async () => {
	try {
		return uuid();
	} catch (error) {
		throw new Error(`Error occurred while generating token: ${error.message}`);
	}
};

export const updateUserToken = async (id_user, token) => {
	try {
		return await prismaClient.user.update({
			where: {
				id_user: id_user,
			},
			data: {
				token: token,
			},
		});
	} catch (error) {
		throw new Error(`Error occurred while updating user token: ${error.message}`);
	}
};

export const searchPaketById = async (id) => {
	try {
		return await prismaClient.paket.findUnique({
			where: {
				id_paket: id,
			},
		});
	} catch (error) {
		throw new Error(`Error occurred while searching paket by id: ${error.message}`);
	}
};

export const searchOrderIdByEmail = async (email) => {
	try {
		return await prismaClient.order.findFirst({
			where: {
				email: email,
			},
		});
	} catch (error) {
		throw new Error(`Error occurred while searching order by email: ${error.message}`);
	}
};

export const searchTeknisiByNama = async (nama_teknisi) => {
	try {
		return await prismaClient.teknisi.findFirst({
			where: {
				nama_teknisi: nama_teknisi,
			},
		});
	} catch (error) {
		throw new Error(`Error occurred while searching teknisi by nama_teknisi: ${error.message}`);
	}
};
