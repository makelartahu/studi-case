import dotenv from 'dotenv';
import { web } from './application/web.js';

dotenv.config();

const init = async () => {
  try {
    web.listen(process.env.PORT, () => {
      console.log(`Server is running on port ${process.env.PORT}`);
    });
  } catch (error) {
    console.error(`Error occurred: ${error.message}`);
  }
}

init();