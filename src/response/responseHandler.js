class ResponseHandler {
  static success(res, data) {
    res.status(200).json({
      code: 0,
      info: "OK",
      data: data,
    });
  }

  static error(res, message, status = 500, data) {
    res.status(status).json({
      code: 1,
      info: "Error",
      message: message,
      data: data,
    });
  }
}

export { ResponseHandler };
