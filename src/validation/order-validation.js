import Joi from "joi";

export const orderValidation = Joi.object({
	nama: Joi.string().max(100).required(),
	email: Joi.string().email().required(),
	upload_identitas: Joi.string().required(),
	kota: Joi.string().max(100).required(),
	kecamatan: Joi.string().max(100).required(),
	jalan: Joi.string().max(100).required(),
	paket_id: Joi.number().required(),
});
export const orderUpdateValidation = Joi.object({
	nama: Joi.string().max(100),
	email: Joi.string().email(),
	upload_identitas: Joi.string(),
	kota: Joi.string().max(100),
	kecamatan: Joi.string().max(100),
	jalan: Joi.string().max(100),
	paket_id: Joi.number(),
	status_id: Joi.number(),
	nama_teknisi: Joi.string(),
	reject_reason: Joi.string().max(100),
});
export const orderSearchValidation = Joi.object({
	id: Joi.number().required(),
});
