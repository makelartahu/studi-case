export const validate = (schema, request) => {
  const result = schema.validate(request, {
    abortEarly: false,
    allowUnknown: true,
  })

  if (result.error) {
    const errors = result.error.details.map((error) => error.message)
    throw new Error(errors.join(', '));
  } else {
    return result.value;
  }
}