import Joi from "joi";

export const userValidation = Joi.object({
	username: Joi.string().max(18).required(),
	password: Joi.string().min(6).required(),
	nama: Joi.string().max(100).required(),
	roleId: Joi.number().required(),
});

export const loginValidation = Joi.object({
	username: Joi.string().required(),
	password: Joi.string().required(),
});

export const userLogoutValidation = Joi.object({
	token: Joi.string().required(),
});
