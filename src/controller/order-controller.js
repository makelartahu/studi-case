import * as orderService from "../service/order-service.js";
import { ResponseHandler } from "../response/responseHandler.js";

export const createOrder = async (req, res, next) => {
	try {
		const token = req.user.token;
		const result = await orderService.createOrder(req.body, token);
		ResponseHandler.success(res, result);
	} catch (error) {
		next(error);
	}
};

export const updateOrder = async (req, res, next) => {
	try {
		const result = await orderService.updateOrder(req.body);
		ResponseHandler.success(res, result);
	} catch (error) {
		next(error);
	}
};

export const getOrder = async (req, res, next) => {
	try {
		const result = await orderService.getOrder(req.body);
		ResponseHandler.success(res, result);
	} catch (error) {
		next(error);
	}
};

export const getOrderList = async (req, res, next) => {
	try {
		const result = await orderService.getOrderList();
		ResponseHandler.success(res, result);
	} catch (error) {
		next(error);
	}
};
