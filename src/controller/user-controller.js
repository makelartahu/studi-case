import * as userService from "../service/user-service.js";
import { ResponseHandler } from "../response/responseHandler.js";

export const createUser = async (req, res, next) => {
	try {
		const result = await userService.createUser(req.body);
		ResponseHandler.success(res, result);
	} catch (error) {
		next(error);
	}
};

export const loginUser = async (req, res, next) => {
	try {
		const result = await userService.loginUser(req.body);
		ResponseHandler.success(res, result);
	} catch (error) {
		next(error);
	}
};

export const getUserList = async (req, res, next) => {
	try {
		const token = req.user.token;
		const result = await userService.getUserByToken(token);
		ResponseHandler.success(res, result);
	} catch (error) {
		next(error);
	}
};

export const logoutUser = async (req, res, next) => {
	try {
		const result = await userService.logoutUser(req);
		ResponseHandler.success(res, result);
	} catch (error) {
		next(error);
	}
};
