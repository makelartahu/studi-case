import { ResponseHandler } from "../response/responseHandler.js";
import * as teknisiService from "../service/teknisi-service.js";

export const getTeknisi = async (req, res, next) => {
	try {
		const result = await teknisiService.getTeknisi(req.body);
		ResponseHandler.success(res, result);
	} catch (error) {
		next(error);
	}
};

export const getTeknisiList = async (req, res, next) => {
	try {
		const result = await teknisiService.getTeknisiList();
		ResponseHandler.success(res, result);
	} catch (error) {
		next(error);
	}
};
