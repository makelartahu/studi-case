import { ResponseHandler } from "../response/responseHandler.js";
import * as paketService from "../service/paket-service.js";

export const getPaketList = async (req, res, next) => {
	try {
		const result = await paketService.getPaketList();
		ResponseHandler.success(res, result);
	} catch (error) {
		next(error);
	}
};
export const getPaketListDesc = async (req, res, next) => {
	try {
		const result = await paketService.getPaketListDesc();
		ResponseHandler.success(res, result);
	} catch (error) {
		next(error);
	}
};
