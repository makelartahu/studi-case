import express from "express";
import * as userController from "../controller/user-controller.js";
import * as paketController from "../controller/paket-controller.js  ";
import * as orderController from "../controller/order-controller.js";
import * as teknisiController from "../controller/teknisi-controller.js";
import { authMiddleware } from "../middleware/auth-middleware.js";

const publicRouter = express.Router();

publicRouter.post("/api/user/register", userController.createUser);
publicRouter.post("/api/user/login", userController.loginUser);
publicRouter.post("/api/user/logout", userController.logoutUser);
publicRouter.post("/api/paket", paketController.getPaketList);
publicRouter.post("/api/paket-desc", paketController.getPaketListDesc);
publicRouter.post("/api/teknisi", teknisiController.getTeknisi);
publicRouter.post("/api/teknisi/list", teknisiController.getTeknisiList);

publicRouter.use(authMiddleware);
publicRouter.post("/api/user", userController.getUserList);
publicRouter.post("/api/order/create", orderController.createOrder);
publicRouter.post("/api/order/update", orderController.updateOrder);
publicRouter.post("/api/order", orderController.getOrder);
publicRouter.post("/api/order/list", orderController.getOrderList);

export { publicRouter };
